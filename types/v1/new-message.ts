export interface INewMessage {
    content: string;
    channelId: number;
    photos_ids: number[],
    files_ids: number[]
}