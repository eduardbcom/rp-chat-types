export interface IChannelMember {
    id: number;
    name: string;
    icon?: string;
    isOwner: boolean;
    type: 'student' | 'teacher' | 'org';
    subType: string;
}
