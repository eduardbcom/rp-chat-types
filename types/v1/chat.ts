import { IChannel } from "./channel";

export interface IChat {
    id: number;
    title: string;
    ownerId: number;
    createdAt: number;
    channels: IChannel[];
}
