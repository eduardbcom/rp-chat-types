export interface IUnreadChannel {
    chatId: number;
    channelId: number;
}