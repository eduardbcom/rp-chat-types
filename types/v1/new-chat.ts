export interface INewChat {
    title: string;
    participants: number[];
}