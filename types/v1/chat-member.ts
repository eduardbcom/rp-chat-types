export interface IChatMember {
    id: number;
    name: string;
    icon?: string;
    isOwner: boolean;
    type: 'student' | 'teacher' | 'org';
    subType: string;
}
