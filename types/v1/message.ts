export interface IMessage {
    id: number;
    content: string;
    ownerId: number;
    ownerName: string;
    ownerImage: string;
    createdAt: number;
    updatedAt: number;
    chatId: number;
    channelId: number;
}
