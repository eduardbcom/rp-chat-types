export interface IChannelDetails {
    id: number;
    title: string;
    amountOfMembers: number;
    chatId: number;
    chatTitle: string;
    ownerId: number;
    isDeleted: boolean;
}
