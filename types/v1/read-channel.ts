export interface IReadChannel {
    chatId: number;
    channelId: number;
}