import { IMessage } from "./message";

export interface IChannel {
    id: number;
    title: string;
    ownerId: number;
    createdAt: number;
    chatId: number;
    message?: IMessage;
    isUnread: boolean;
    isDeleted: boolean;
}
