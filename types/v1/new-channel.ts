export interface INewChannel {
    title: string;
    participants: number[];
    chatId: number;
}
